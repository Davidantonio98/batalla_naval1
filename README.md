using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Batalla_Naval
{
    class Program
    {
       public static void Main(string[] args)
        {
            Random rnd = new Random();
            int horizontal, columna;
            int[,] mar = new int[10, 10];

            for (int i=0; i <10; i++)
            
                for (int j = 0; j < 10; j++)
                
                    mar[i, j] = 0;

            Console.WriteLine("\n\n Ingresa en direccion horizontal el barco compa [1,10]:");
            horizontal = int.Parse(Console.ReadLine());

            if (horizontal >= 1 && horizontal <= 10)
            {
                columna = rnd.Next(0, 6);
                for (int i = 0; i < 4; i++)
                    mar[horizontal - 1, columna + i] = 1;

                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                        Console.Write(mar[i, j] + "\t");
                    Console.Write("\n");
                }

            }
            else
                Console.Write("\n Ingresa un espacio que sea valido [i,10]");

            Console.ReadKey();
            
        }
    }
}